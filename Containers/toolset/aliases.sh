 # Commonly Used Aliases
alias ls="ls -hal"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias cls="clear && ls"

# Always enable colored `grep` output
# Note: `GREP_OPTIONS="--color=auto"` is deprecated, hence the alias usage.
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Laravel / artisan
alias art="php artisan"
alias artisan="php artisan"
alias cdump="composer dump-autoload -o"
alias composer:dump="composer dump-autoload -o"
alias db:reset="php artisan migrate:reset && php artisan migrate --seed"
alias fresh="php artisan migrate:fresh"
alias migrate="php artisan migrate"
alias db:refresh="php artisan migrate:refresh"
alias migrate:refresh="php artisan migrate:refresh"
alias db:rollback="php artisan migrate:rollback"
alias migrate:rollback="php artisan migrate:rollback"
alias seed="php artisan:seed"

# PHP Unit
alias phpunit="./vendor/bin/phpunit"
alias pu="phpunit"
alias puf="phpunit --filter"
alias pud='phpunit --debug'

# Gulp
alias g="gulp"
alias gw="gulp watch"
alias watch="gulp watch"

# Git
alias gc="git commit -am"
alias gs="git status"
alias gl="git pull"
alias gp="git push"
alias ga="git add ."
alias gc="git checkout"
alias gcb="git checkout -b"
alias gwhoops="git reset --hard && git clean -df"

#Rocketeer
alias rocket="/root/.composer/vendor/anahkiasen/rocketeer/bin/rocketeer"
alias rocketeer="/root/.composer/vendor/anahkiasen/rocketeer/bin/rocketeer"
