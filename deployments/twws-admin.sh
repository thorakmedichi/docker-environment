#!/bin/bash

# Mysql
sudo docker exec -it mysql /usr/bin/mysql -e 'CREATE DATABASE IF NOT EXISTS `tw01-admin` CHARACTER SET utf8 COLLATE utf8_general_ci;'
sudo docker exec -it mysql /usr/bin/mysql -e 'GRANT ALL ON `tw01-admin`.* TO "travelers"@"%" IDENTIFIED BY "secret";'

# Toolset
sudo docker exec -it toolset bash -c "cd /projects/bios/twws/admin \
    && git pull origin master \
    && chmod -R 777 storage \
    && composer install && npm install \
    && composer dump-autoload \
    && php artisan migrate && php artisan db:seed \
    && gulp"


