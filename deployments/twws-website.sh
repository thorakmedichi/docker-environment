#!/bin/bash

# Mysql
sudo docker exec -it mysql /usr/bin/mysql -e 'CREATE DATABASE IF NOT EXISTS `tw01-dev` CHARACTER SET utf8 COLLATE utf8_general_ci;'
sudo docker exec -it mysql /usr/bin/mysql -e 'GRANT ALL ON `tw01-dev`.* TO "travelers"@"%" IDENTIFIED BY "secret";'

# Toolset
sudo docker exec -it toolset bash -c "cd /projects/bios/twws/website \
    && git pull origin master \
    && chmod -R 777 craft/config \
    && chmod -R 777 storage \
    && composer install && npm install \
    && composer dump-autoload \
    && gulp"


tput setaf 2;
printf "\n\nNext Steps:\n\n
1) Update your carft/config/db.php file so that 'server' => 'mysql'\n
2) Update your .env file so that the your database variables are as follows\n
    DB_CONNECTION=mysql \n
    DB_HOST=127.0.0.1 \n
    DB_PORT=3306 \n
    DB_NAME=tw01-dev \n
    DB_USER=travelers \n
    DB_PASSWORD=secret \n
3) Go to yourtravelersdomain.whatever/admin/install \n
    use the credentials supplied by bios to create the initial account \n
4) Copy the new sql dump into your new db
\n\n"
tput sgr0

