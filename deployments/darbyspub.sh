#!/bin/bash

# Mysql
sudo docker exec -it mysql /usr/bin/mysql -e 'CREATE DATABASE IF NOT EXISTS `darbyspub` CHARACTER SET utf8 COLLATE utf8_general_ci;'
sudo docker exec -it mysql /usr/bin/mysql -e 'GRANT ALL ON `darbyspub`.* TO "darbyspub"@"%" IDENTIFIED BY "derbypass";'

# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/database.sql"
# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/dpws-71.sql"
# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/dpws-82.sql"
# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/dpws-94.sql"
# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/dpws-104.sql"
# sudo docker exec -it mysql bash -c "mysql darbyspub < /projects/bios/darbyspub/resources/data.sql"