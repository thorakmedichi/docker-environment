#!/bin/bash

# Mysql
sudo docker exec -it mysql /usr/bin/mysql -e 'CREATE DATABASE IF NOT EXISTS `gofindme_admin` CHARACTER SET utf8 COLLATE utf8_general_ci;'
sudo docker exec -it mysql /usr/bin/mysql -e 'GRANT ALL ON `gofindme_admin`.* TO "sketchpad"@"%" IDENTIFIED BY "87y1t1EerJpl";'

# Toolset
sudo docker exec -it toolset bash -c "cd /projects/personal/gofindme-admin \
    && git pull origin master \
    && composer install && npm install \
    && composer dump-autoload \
    && php artisan migrate && php artisan db:seed \
    && gulp"